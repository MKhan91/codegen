

let javaDataTypesMap = { "string": "String", "number": "Long", "boolean": "Boolean" };
let finalDataTypesMap = {};
var methodName, basePackage, requestName, responseName, apiName, controllerName, serviceName, controllerUrl;

const newLineCharacter = "\r\n";

const apiImports = "import com.tmobile.qvxpmu.UserDetails;" + newLineCharacter +
    "import org.springframework.cloud.context.config.annotation.RefreshScope;" + newLineCharacter +
    "import org.springframework.security.access.prepost.PreAuthorize;" + newLineCharacter +
    "import org.springframework.web.bind.annotation.ModelAttribute;" + newLineCharacter +
    "import org.springframework.web.bind.annotation.PostMapping;" + newLineCharacter +
    "import org.springframework.web.bind.annotation.RequestBody;" + newLineCharacter +
    "import org.springframework.web.bind.annotation.RequestMapping;" + newLineCharacter +
    "import org.springframework.web.bind.annotation.RestController;" + newLineCharacter +
    "import javax.validation.Valid;";

const controllerImports = "import com.tmobile.qvxpmu.UserDetails" + newLineCharacter +
    "import org.springframework.web.bind.annotation.RestController" + newLineCharacter;



$(document).ready(function () {

    $("#download").bind("click", function () {
        let serviceName = $("#serviceName").val();
        download(serviceName + "Request.groovy", $("#requestPojo").text());
        setTimeout(function () {
            download(serviceName + "Response.groovy", $("#responsePojo").text());
        }, 1000);
        setTimeout(function () {
            download(serviceName + "Api.java", $("#api").text());
        }, 2000);
        setTimeout(function () {
            download(serviceName + "Controller.groovy", $("#controller").text());
        }, 3000);
        setTimeout(function () {
            download(serviceName + "Service.groovy", $("#service").text());
        }, 4000);
    });

    $("#requestPojoBtn").bind("click", function () {
        let serviceName = $("#requestName").val();
        download(serviceName + ".groovy", $("#requestPojo").text());
    });

    $("#responsePojoBtn").bind("click", function () {
        let serviceName = $("#responseName").val();
        download(serviceName + ".groovy", $("#responsePojo").text());
    });

    $("#apiBtn").bind("click", function () {
        let serviceName = $("#apiName").val();
        download(serviceName + ".groovy", $("#api").text());
    });

    $("#controllerBtn").bind("click", function () {
        let serviceName = $("#controllerName").val();
        download(serviceName + ".groovy", $("#controller").text());
    });
    $("#serviceBtn").bind("click", function () {
        let serviceName = $("#serviceName").val();
        download(serviceName + ".groovy", $("#service").text());
    });
    $("#controllerTestBtn").bind("click", function () {
        let serviceName = $("#controllerName").val();
        download(serviceName + "Test.groovy", $("#controllerTest").text());
    });
    $("#serviceTestBtn").bind("click", function () {
        let serviceName = $("#serviceName").val();
        download(serviceName + "Test.groovy", $("#serviceTest").text());
    });
    $("#generate, #updateMethodNames").bind("click", function () {
        buildJavaText();
        $("#download, .removeDownloadDisable").removeAttr("disabled");
    });

    $("#baseName").bind("keyup", function () {
        let currentVal = $(this).val();
        updateNames(currentVal);
    });
    
    setTimeout(function(){ 
        updateNames($("#baseName").val());
    }, 1000);

});

function updateNames(currentVal){
        $("#methodName").val(currentVal);
        $("#requestName").val(currentVal+"Request");
        $("#responseName").val(currentVal+"Response");
        $("#apiName").val(currentVal+"Api");
        $("#controllerName").val(currentVal+"Controller");
        $("#serviceName").val(currentVal+"Service");
        $("#controllerUrl").val("/"+lowerise(currentVal));
}

function updateMethodNames(){
    methodName = $("#methodName").val();
    basePackage = $("#basePackage").val();
    requestName = capitalize($("#requestName").val());
    responseName = capitalize($("#responseName").val());
    apiName = capitalize($("#apiName").val());
    controllerName = capitalize($("#controllerName").val());
    serviceName =capitalize($("#serviceName").val());
    controllerUrl = $("#controllerUrl").val();
}

function buildJavaApi(serviceName) {
    let resultStr = "package "+basePackage+".controller;" + newLineCharacter + newLineCharacter;
    resultStr += getPojosImportStr();
    resultStr += apiImports + newLineCharacter + newLineCharacter;
    resultStr += "@RefreshScope" + newLineCharacter +
    "@RestController" + newLineCharacter +
    "@RequestMapping(\""+controllerUrl+"\") " + newLineCharacter + newLineCharacter;;
    resultStr += "public interface " + apiName + " {" + newLineCharacter + newLineCharacter;
    resultStr += "\t@PostMapping(value = \"" + serviceName + "\")" + newLineCharacter;
    resultStr += "\t@PreAuthorize(\"hasAuthority('RATE_PLANS_AND_FEATURES:CHANGE_RATE_PLAN') && hasAuthority('RATE_PLANS_AND_FEATURES:ADD_SOC')\")" + newLineCharacter;
    resultStr += "\t" + responseName + " " + methodName + "(@RequestBody @Valid " + requestName + " " + lowerise(requestName) + ", @ModelAttribute UserDetails userDetails);" + newLineCharacter;
    resultStr += "}";
    $("#api").html(resultStr);
}


function buildJavaController(serviceName) {
    let resultStr = "package "+basePackage+".controller.impl" + newLineCharacter + newLineCharacter;
    let serviceNameVariable = lowerise(serviceName);
    let requestPojoVariable = lowerise(requestName);
    resultStr += "import "+basePackage+".controller." + apiName + newLineCharacter;
    resultStr += getPojosImportStr();
    resultStr += "import "+basePackage+".service." + serviceName  + newLineCharacter;
    resultStr += controllerImports + newLineCharacter + newLineCharacter;
    resultStr += "@RestController " + newLineCharacter + "class " + controllerName + " implements " + apiName + "{" + newLineCharacter + newLineCharacter;
    resultStr += "\t" + serviceName + " " + serviceNameVariable + newLineCharacter + newLineCharacter;
    resultStr += "\t" + controllerName + "(" + serviceName + " " + serviceNameVariable + "){" + newLineCharacter;
    resultStr += "\t\t" + "this." + serviceNameVariable + " = " + serviceNameVariable + "" + newLineCharacter;
    resultStr += "\t}" + newLineCharacter;
    resultStr += "\t" + responseName + " " + methodName + "(" + requestName + " " + requestPojoVariable + ", UserDetails userDetails) {" + newLineCharacter;
    resultStr += "\t\treturn " + serviceNameVariable + "." + methodName + "(" + requestPojoVariable + ", userDetails)" + newLineCharacter;
    resultStr += "\t}" + newLineCharacter;
    resultStr += "}";
    $("#controller").html(resultStr);
}

function buildJavaService(serviceName) {
    let requestPojoVariable = lowerise(serviceName);
    let resultStr = "package "+basePackage+".service" + newLineCharacter + newLineCharacter;
    resultStr += getPojosImportStr();
    resultStr += "import com.tmobile.qvxpmu.UserDetails" + newLineCharacter;
    resultStr += "import org.springframework.stereotype.Service" + newLineCharacter + newLineCharacter + newLineCharacter;
    resultStr += "@Service "+newLineCharacter+"class " + serviceName + " extends BaseService {" + newLineCharacter + newLineCharacter;
    resultStr += "\t" + responseName +" "+ methodName + "(" + requestName + " " + requestPojoVariable + ", UserDetails userDetails) {" + newLineCharacter;
    let responseStr = JSON.stringify($("#response").val());
    responseStr = responseStr.replaceAll("$","");
    resultStr += "\t\tfinal String MOCK_RESPONSE = " + responseStr + newLineCharacter;
    resultStr += "\t\treturn getObjectFrom(MOCK_RESPONSE," + responseName + ".class)" + newLineCharacter;
    resultStr += "\t}" + newLineCharacter + "}";
    $("#service").html(resultStr);
}

function buildJavaControllerTest() {

}

function buildJavaServiceTest() {

}


function getPojosImportStr(){
    let resultStr = "";
    resultStr += "import "+basePackage+".model." + requestName+ ";"  + newLineCharacter;
    resultStr += "import "+basePackage+".model." + responseName + ";" + newLineCharacter;
    return resultStr;
}



function buildJavaText() {
    updateMethodNames();
    let uiRequestJson = JSON.parse($("#request").val());
    let uiResponseJson = JSON.parse($("#response").val());

    let serviceName = $("#serviceName").val();
    let classFieldsMap = buildClassFieldsMap(uiRequestJson)
    console.log("request pojo fields", classFieldsMap);
    let requsetPojoStr = buildJavaPojo(requestName, classFieldsMap, []);
    let modelPackage = 'package '+ basePackage+'.model' + newLineCharacter + newLineCharacter;
    $("#requestPojo").html(modelPackage + requsetPojoStr);

    classFieldsMap = undefined;
    classFieldsMap = buildClassFieldsMap(uiResponseJson);
    console.log("response pojo fields", JSON.stringify(classFieldsMap));
    let responsePojoStr = buildJavaPojo(responseName, classFieldsMap, []);
    $("#responsePojo").html(modelPackage + responsePojoStr);

    buildJavaApi(serviceName);
    buildJavaController(serviceName);
    buildJavaService(serviceName);

}



function buildClassFieldsMap(json, classFiledMap) {
    if (json) {
        classFiledMap = !classFiledMap ? {} : classFiledMap;
        let entries = Object.entries(json);
        entries.forEach(entry => {
            let javaType = getJavaDatatype(entry[1]);
            if (javaType !== "object" && javaType !== "object[]") {
                if (!classFiledMap[javaType]) {
                    classFiledMap[javaType] = [entry[0]];
                } else if (classFiledMap[javaType] && classFiledMap[javaType].indexOf(entry[0]) === -1) {
                    classFiledMap[javaType].push(entry[0]);
                }

            } else if (javaType === "object") {
                classFiledMap[entry[0]] = buildClassFieldsMap(entry[1], classFiledMap[entry[0]])
            } else if (javaType === "object[]") {
                entry[1].forEach(subentry => {
                    classFiledMap['array_is_dangerous____' + entry[0]] = buildClassFieldsMap(subentry, classFiledMap['array_is_dangerous____' + entry[0]])
                });
            }
        });
        return classFiledMap;
    }
}


function getJavaDatatype(val) {
    let result = javaDataTypesMap[typeof val];
    if (result) {
        return result;
    } else if (!result) {
        if (isArray(val)) {
            if (val.length > 0) {
                let arrayType = typeof val[0];
                if (javaDataTypesMap[arrayType]) {
                    return javaDataTypesMap[arrayType] + "[]";
                } else {
                    return "object[]";
                }
            }
        } else if (isObject(val)) {
            return "object";
        }
    }
}

function buildJavaPojo(className, fieldsJsonObj, classesStrArr) {
    for (key in fieldsJsonObj) {
        if (fieldsJsonObj[key].toString() === '[object Object]') {
            buildJavaPojo(key, fieldsJsonObj[key], classesStrArr);
        }
    }
    let classStrTmp = buildClassStr(className, fieldsJsonObj);
    classesStrArr.push(classStrTmp);
    return classesStrArr.toString().replace(/,class /g, newLineCharacter + "class ");
}

function buildClassStr(className, fieldsObj) {
    let result = "";
    if (className.startsWith("array_is_dangerous____")) {
        className = className.replace("array_is_dangerous____", "")
    }
    result += "class " + capitalize(className) + "{ " + newLineCharacter;
    for (key in fieldsObj) {
        if ("[object Object]" === fieldsObj[key].toString()) {
            if (key.startsWith("array_is_dangerous____")) {
                key = key.replace("array_is_dangerous____", "")
                result += "\t" + capitalize(key) + "[] " + key + newLineCharacter;
            } else {
                result += "\t" + capitalize(key) + " " + key + newLineCharacter;
            }
        } else {
            result += "\t" + key + " " + fieldsObj[key].toString() + newLineCharacter;
        }
    }
    result += "}" + newLineCharacter;
    return result;
}

function capitalize(word) {
    return word[0].toUpperCase() + word.slice(1);
}

function lowerise(word) {
    return word[0].toLowerCase() + word.slice(1);
}





function download(filename, text) {
    var element = document.createElement('a');
    element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
    element.setAttribute('download', filename);

    element.style.display = 'none';
    document.body.appendChild(element);

    element.click();

    document.body.removeChild(element);
}




function isArray(val) {
    let constructor = val.constructor.toString();
    return "Array" === constructor.slice(9, constructor.indexOf("("));
}

function isObject(val) {
    let constructor = val.constructor.toString();
    return "Object" === constructor.slice(9, constructor.indexOf("("));
}
