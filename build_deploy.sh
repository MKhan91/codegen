#build the project
rm -r ./dist
mkdir -p dist/fcs-tms-codegen
cp -v ./*.js ./dist/fcs-tms-codegen/
cp -v ./*.css ./dist/fcs-tms-codegen/
cp -v ./*.png ./dist/fcs-tms-codegen/
cp -v ./*.html ./dist/fcs-tms-codegen/
#deploy_stage
cf login -a api.sys.px-npe02b.cf.t-mobile.com -o QVXP-microapps-nonprod -s playground
cf push -f manifest.yml
echo "\nApp Deployed Successfully ... !!!"
echo "\nTry opening https://fcs-tms-codegen.apps.px-npe02b.cf.t-mobile.com/v3.html"