# fcs-tms-codegen

TODO:
Test files code generation

Future Upcoming Features
    download all as zip with folder structure
    Web ide (edit code in browser)
    Design Support (Draw.io)
        - UI
        - Server side
        - Database side
    Reader Support (Swagger, wsdl)
    template support (fcs and deepio, others)
    Multi language support (Angular, Python, React, ionic, nodejs, selinium)
    Repository support (push generated code to vcs)
    tool deployment support (deploy generated code)
    Multi Database support ( rdbms, columnar, nosql, graphdb)
    Multi framework support ( spring, hibernate, other langugae frameworks too)
    native apps support (create native apps like apk,ios,etc from generated code)
    service virtualization ( integrate with proxy tool)
    Artificaial Intelligence support ( suggesting user for better options based on requirements)
    Template reader support (read existing projects and generate code accordingly)
    Log Searching functionality ( user freidnly filtering options than splunk )
    File Name validation
    Folder structure creation 
    show mappings of file names from left -> right
    notifications after actions ( clicking buttons )
    integrate with ide, cli
    templates downlodable which should give logging, exceptions